class Calculator extends Element{

  constructor(){
      super();
      this.initDOMElement();
      this._a = null;
      this._b = null;
      this._operant = null;
      this._c;
      this._display = "0";
      this._memory = null;
      this.firstNumer = true;
      this.doMath = {
        '+' : function(a,b){ return parseFloat(a)+parseFloat(b);},
        '-' : function(a,b){ return parseFloat(a)-parseFloat(b);},
        'x' : function(a,b){ return parseFloat(a) * parseFloat(b);}
      }
  }

  set display(value){
    
    this._display = value;
    this.DOMElement.querySelector(".display").innerHTML = this.display;
  }

  get display(){
    return this._display;
  }

  set a(value){
    if(this.firstNumer){
      this._a = value;
    }else{
      this._a+=value;
    }
    this.display = this._a;
    this.firstNumer = false;
  }

  get a(){
    return this._a;
  }

  set b(value){
    if(this.firstNumer){
      this._b = value;
    }else{
      this._b+=value;
    }
    this.display = this._b;
    this.firstNumer = false;
  }

  get b(){
    return this._b;
  }

  set c(value){
    this._c = value;
    this.memory+=this.b;
    this.display = this.c;
  }

  get c(){
    return this._c;
  }

  set operant(value){
    this.checkIfOperable();
    this._operant = value;
    this.firstNumer = true;
    if(this.memory == null){
      this.memory=this.a+this.operant;
    }else{

    }
  }

  get operant(){
    return this._operant;
  }

  set memory(value){
    this._memory = value;
    this.DOMElement.querySelector(".memory").innerHTML = this._memory;
    this.display = 0;
  }

  get memory(){
    return this._memory;
  }

  initDOMElement(){

    let calculator = document.createElement("div");
    calculator.id = "calculator";

    let buttonArray = [
      {value: "",class: "memory"},
      {value: "0",class: "display"},
      {value: "AC",class: "special"},
      {value: "+/-",class: "special"},
      {value: "%",class: "special"},
      {value: "/",class: "orange operant"},
      {value: "7",class: "number"},
      {value: "8",class: "number"},
      {value: "9",class: "number"},
      {value: "x",class: "orange operant"},
      {value: "4",class: "number"},
      {value: "5",class: "number"},
      {value: "6",class: "number"},
      {value: "-",class: "orange operant"},
      {value: "1",class: "number"},
      {value: "2",class: "number"},
      {value: "3",class: "number"},
      {value: "+",class: "orange operant"},
      {value: "0",class: "zero"},
      {value: ",",class: "special comma"},
      {value: "=",class: "orange special"}
    ];

    for (let i = 0; i < buttonArray.length; i++) {
      let el = document.createElement("div");
      el.classList = buttonArray[i].class;
      el.innerHTML = buttonArray[i].value;
      if(!el.classList.contains("display")){//Si es un display, no tiene este listener
        el.onclick = (event) => {
          if(el.classList.contains("number") || el.classList.contains("zero") || el.classList.contains("comma")){
            if(this.operant != null){
              this.dataBind(event,"b");
            }else{
              this.dataBind(event,"a");
            }
          }
          if(el.classList.contains("operant")){
            this.dataBind(event,"operant");
          }
        }
      }
      calculator.appendChild(el);
    }

    this.DOMElement = calculator;
  }

  checkIfOperable(){
    if(this.a != null && this.b != null){
      this.c = this.doMath[this.operant](this.a,this.b);
    }
  }

  dataBind(e,variable){
    this[variable] = e.target.innerHTML;
    //this[`update${variable.charAt(0).toUpperCase() + variable.slice(1)}`]();
  }

}